package com.instant.system.interview.cspada.parking.provider;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.instant.system.interview.cspada.parking.ParkingData;

class ParkingDataProviderBordeauxTest {

    @Test
    void mapFeature() {
        OGCParkingResponse.Feature feature = new OGCParkingResponse.Feature();
        feature.setType("type");
        OGCParkingResponse.Geometry geometry = new OGCParkingResponse.Geometry();
        geometry.setCoordinates(new double[] { -0.5443771, 44.8755207});
        feature.setGeometry(geometry);
        OGCParkingResponse.Properties properties = new OGCParkingResponse.Properties();
        feature.setProperties(properties);
        properties.setNom("Parking name");
        properties.setEtat("FERME");
        properties.setLibres(100);
        properties.setTotal(200);
        properties.setIdent("parking-id");

        ParkingData parkingData = ParkingDataProviderBordeaux.mapFeature(feature);
        assertThat(parkingData.getPosition()).isNotNull();
        assertThat(parkingData.getPosition().getLatitude()).isEqualTo(44.8755207);
        assertThat(parkingData.getPosition().getLongitude()).isEqualTo(-0.5443771);
        assertThat(parkingData.getId()).isEqualTo(properties.getIdent());
        assertThat(parkingData.getName()).isEqualTo(properties.getNom());
        assertThat(parkingData.getTotalSpots()).isEqualTo(properties.getTotal());
        assertThat(parkingData.getFreeSpots()).isEqualTo(properties.getLibres());
    }
}