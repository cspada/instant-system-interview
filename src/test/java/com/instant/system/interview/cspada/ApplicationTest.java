package com.instant.system.interview.cspada;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.instant.system.interview.cspada.geoloc.Position;
import com.instant.system.interview.cspada.parking.ParkingData;
import com.instant.system.interview.cspada.parking.ParkingState;
import com.instant.system.interview.cspada.parking.provider.OGCParkingResponse;
import com.instant.system.interview.cspada.webclient.WebClientHelper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApplicationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private WebClientHelper webClientHelper;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void listParkings() throws IOException {
        // Read mocked response from json file
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("geojson-response.json");
        OGCParkingResponse mockedOGCResponse = objectMapper.readValue(resourceAsStream, OGCParkingResponse.class);

        when(webClientHelper.getBlocking(anyString(), anyString(), any()))
                .thenReturn(mockedOGCResponse);

        ParkingData[] response = restTemplate
                .getForEntity("http://localhost:" + port + "/parkings/bordeaux", ParkingData[].class)
                .getBody();

        ParkingData expected1 = parkingData(44.8755207, -0.5443771, "CUBPK44", "Parc-Relais Brandenburg", 211, 132,
                ParkingState.CLOSED);
        ParkingData expected2 = parkingData(44.8561424, -0.5618781, "CUBPK14", "Bord'Eau Village", 364, 235,
                ParkingState.OPEN);

        assertThat(response).containsExactlyInAnyOrder(expected1, expected2);
    }

    private ParkingData parkingData(double latitude, double longitude, String id, String name, int total, int free,
            ParkingState state) {
        ParkingData parkingData = new ParkingData();
        parkingData.setPosition(new Position(latitude, longitude));
        parkingData.setId(id);
        parkingData.setState(state);
        parkingData.setName(name);
        parkingData.setTotalSpots(total);
        parkingData.setFreeSpots(free);
        return parkingData;
    }
}
