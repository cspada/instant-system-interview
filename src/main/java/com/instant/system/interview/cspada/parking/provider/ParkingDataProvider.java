package com.instant.system.interview.cspada.parking.provider;

import java.util.List;

import com.instant.system.interview.cspada.parking.ParkingData;

public interface ParkingDataProvider {

    /**
     * Returns all the {@link ParkingData} known for this provider;
     */
    List<ParkingData> listParkings();
}
