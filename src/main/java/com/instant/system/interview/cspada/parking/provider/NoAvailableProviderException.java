package com.instant.system.interview.cspada.parking.provider;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown to indicate than no {@link ParkingDataProvider} is available for a {@link ProviderCriteria}.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class NoAvailableProviderException extends Exception {

    public NoAvailableProviderException(String message) {
        super(message);
    }
}
