package com.instant.system.interview.cspada.parking;

public enum ParkingState {

    OPEN,
    CLOSED
}
