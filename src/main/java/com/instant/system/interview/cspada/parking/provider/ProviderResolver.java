package com.instant.system.interview.cspada.parking.provider;

/**
 * Resolves which provider should be use to query parking data.
 */
public interface ProviderResolver {

    /**
     * Get a {@link ParkingDataProvider} depending of the given criteria.
     */
    ParkingDataProvider getProvider(ProviderCriteria criteria) throws NoAvailableProviderException;
}
