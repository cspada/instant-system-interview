package com.instant.system.interview.cspada.parking.provider;

import java.util.StringJoiner;

/**
 * Criteria to resolve which {@link ParkingDataProvider} to use.
 */
public class ProviderCriteria {

    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ProviderCriteria.class.getSimpleName() + "[", "]")
                .add("city='" + city + "'")
                .toString();
    }
}
