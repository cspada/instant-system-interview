package com.instant.system.interview.cspada.parking;

import java.util.Objects;

import com.instant.system.interview.cspada.geoloc.Position;

public class ParkingData {

    private Position position;
    private String id;
    private String name;
    private int freeSpots;
    private int totalSpots;
    private ParkingState state;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFreeSpots() {
        return freeSpots;
    }

    public void setFreeSpots(int freeSpots) {
        this.freeSpots = freeSpots;
    }

    public int getTotalSpots() {
        return totalSpots;
    }

    public void setTotalSpots(int totalSpots) {
        this.totalSpots = totalSpots;
    }

    public ParkingState getState() {
        return state;
    }

    public void setState(ParkingState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParkingData that = (ParkingData) o;
        return freeSpots == that.freeSpots && totalSpots == that.totalSpots && Objects
                .equals(position, that.position) && Objects.equals(id, that.id) && Objects
                .equals(name, that.name) && state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, id, name, freeSpots, totalSpots, state);
    }
}
