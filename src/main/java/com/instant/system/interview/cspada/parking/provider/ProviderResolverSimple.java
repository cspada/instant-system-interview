package com.instant.system.interview.cspada.parking.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Simple implementation of a {@link ProviderResolver} that return
 */
@Service
public class ProviderResolverSimple implements ProviderResolver {

    @Autowired
    private ParkingDataProviderBordeaux parkingDataProviderBordeaux;

    @Override
    public ParkingDataProvider getProvider(ProviderCriteria criteria) throws NoAvailableProviderException {
        if (criteria.getCity().equalsIgnoreCase(ParkingDataProviderBordeaux.CITY_NAME)) {
            return parkingDataProviderBordeaux;
        }
        throw new NoAvailableProviderException("No available provider for criteria " + criteria);
    }
}
