package com.instant.system.interview.cspada.webclient;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

@Service
public class WebClientHelper {

    private WebClient.Builder webClientBuilder;

    public WebClientHelper(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    public <T> T getBlocking(String baseUrl, String endpoint, Class<T> returnType) {
        WebClient webClient = webClientBuilder.baseUrl(baseUrl).build();
        Mono<T> responseMono = webClient.get().uri(endpoint).retrieve()
                .bodyToMono(returnType);
        return responseMono.block();
    }
}
