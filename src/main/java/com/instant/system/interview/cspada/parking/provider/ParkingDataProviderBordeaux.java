package com.instant.system.interview.cspada.parking.provider;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.instant.system.interview.cspada.geoloc.Position;
import com.instant.system.interview.cspada.parking.ParkingData;
import com.instant.system.interview.cspada.parking.ParkingState;
import com.instant.system.interview.cspada.webclient.WebClientHelper;

/**
 * {@link ParkingDataProvider} implementation returning the parking data for the Bordeaux city.
 */
@Service
public class ParkingDataProviderBordeaux implements ParkingDataProvider {

    public static final String CITY_NAME = "Bordeaux";

    // As this is a simple project, the base url and query endpoint are hard coded and not exported as
    // aplication configuration.
    private static final String BASE_URL = "http://data.lacub.fr";
    private static final String ENDPOINT = "/geojson?key=9Y2RU3FTE8&version=1.1.0&request=GetFeature&typename=ST_PARK_P&srsname=EPSG:4326";

    private WebClientHelper webClientHelper;

    public ParkingDataProviderBordeaux(WebClientHelper webClientHelper) {
        this.webClientHelper = webClientHelper;
    }

    @Override
    public List<ParkingData> listParkings() {
        return map(webClientHelper.getBlocking(BASE_URL, ENDPOINT, OGCParkingResponse.class));
    }

    public static List<ParkingData> map(OGCParkingResponse response) {
        if (response == null || response.getFeatures() == null) {
            return Collections.emptyList();
        } else {
            return response.getFeatures().stream()
                    .map(ParkingDataProviderBordeaux::mapFeature)
                    .collect(Collectors.toList());
        }
    }

    public static ParkingData mapFeature(OGCParkingResponse.Feature feature) {
        ParkingData parkingData = new ParkingData();
        // The json response returns the goe point using  "coordinates": [-0.5443771, 44.8755207]
        // Longitude seems to be returned at index 1, ond longitude at index 0
        parkingData.setPosition(
                new Position(feature.getGeometry().getCoordinates()[1], feature.getGeometry().getCoordinates()[0]));
        OGCParkingResponse.Properties properties = feature.getProperties();
        parkingData.setId(properties.getIdent());
        parkingData.setName(properties.getNom());
        parkingData.setState(mapState(properties.getEtat()));
        parkingData.setTotalSpots(properties.getTotal());
        parkingData.setFreeSpots(properties.getLibres());
        return parkingData;
    }

    private static ParkingState mapState(String value) {
        return "FERME".equalsIgnoreCase(value) ? ParkingState.CLOSED : ParkingState.OPEN;
    }
}
