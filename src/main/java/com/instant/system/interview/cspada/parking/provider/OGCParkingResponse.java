package com.instant.system.interview.cspada.parking.provider;

import java.util.List;

public class OGCParkingResponse {

    private String type;
    private List<Feature> features;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(
            List<Feature> features) {
        this.features = features;
    }

    public static class Feature {
        private String type;
        private Geometry geometry;
        private Properties properties;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Geometry getGeometry() {
            return geometry;
        }

        public void setGeometry(Geometry geometry) {
            this.geometry = geometry;
        }

        public Properties getProperties() {
            return properties;
        }

        public void setProperties(Properties properties) {
            this.properties = properties;
        }
    }

    public static class Geometry {
        private String type;
        private double[] coordinates;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public double[] getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(double[] coordinates) {
            this.coordinates = coordinates;
        }
    }

    public static class Properties {
        private String gid;
        private int geom_o;
        private String geom_err;
        private String ident;
        private String nom;
        private int libres;
        private int total;
        private int prepaye;
        private String etat;
        private int connecte;
        private String url;
        private Double ta_titul;
        private Double ta_ntitul;
        private Double th_quar;
        private Double th_demi;
        private Double th_heur;
        private Double th_2;
        private Double th_3;
        private Double th_4;
        private Double th_10;
        private Double th_24;
        private Double th_nuit;
        private Double ta_resmoi;
        private Double ta_nres7j;
        private Double ta_moimot;
        private Double ta_moivel;
        private Double tv_1h;
        private String infor;
        private String propr;
        private String titul;
        private String exploit;
        private String typgest;
        private String secteur;
        private String insee;
        private String an_serv;
        private String type;
        private int nb_niv;
        private int np_hginf;
        private int np_hgsup;
        private int np_fourr;
        private int np_global;
        private int np_total;
        private int np_pr;
        private int np_pmr;
        private int np_vle;
        private int np_mobalt;
        private int np_covoit;
        private int np_stlav;
        private int np_2rmot;
        private int np_2rele;
        private int np_veltot;
        private int np_velec;
        private String adresse;
        private Double gabari_max;
        private Double gabari_std;
        private String ta_type;
        private String ta_handi;
        private String siret;

        public String getGid() {
            return gid;
        }

        public void setGid(String gid) {
            this.gid = gid;
        }

        public int getGeom_o() {
            return geom_o;
        }

        public void setGeom_o(int geom_o) {
            this.geom_o = geom_o;
        }

        public String getGeom_err() {
            return geom_err;
        }

        public void setGeom_err(String geom_err) {
            this.geom_err = geom_err;
        }

        public String getIdent() {
            return ident;
        }

        public void setIdent(String ident) {
            this.ident = ident;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public int getLibres() {
            return libres;
        }

        public void setLibres(int libres) {
            this.libres = libres;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPrepaye() {
            return prepaye;
        }

        public void setPrepaye(int prepaye) {
            this.prepaye = prepaye;
        }

        public String getEtat() {
            return etat;
        }

        public void setEtat(String etat) {
            this.etat = etat;
        }

        public int getConnecte() {
            return connecte;
        }

        public void setConnecte(int connecte) {
            this.connecte = connecte;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Double getTa_titul() {
            return ta_titul;
        }

        public void setTa_titul(Double ta_titul) {
            this.ta_titul = ta_titul;
        }

        public Double getTa_ntitul() {
            return ta_ntitul;
        }

        public void setTa_ntitul(Double ta_ntitul) {
            this.ta_ntitul = ta_ntitul;
        }

        public Double getTh_quar() {
            return th_quar;
        }

        public void setTh_quar(Double th_quar) {
            this.th_quar = th_quar;
        }

        public Double getTh_demi() {
            return th_demi;
        }

        public void setTh_demi(Double th_demi) {
            this.th_demi = th_demi;
        }

        public Double getTh_heur() {
            return th_heur;
        }

        public void setTh_heur(Double th_heur) {
            this.th_heur = th_heur;
        }

        public Double getTh_2() {
            return th_2;
        }

        public void setTh_2(Double th_2) {
            this.th_2 = th_2;
        }

        public Double getTh_3() {
            return th_3;
        }

        public void setTh_3(Double th_3) {
            this.th_3 = th_3;
        }

        public Double getTh_4() {
            return th_4;
        }

        public void setTh_4(Double th_4) {
            this.th_4 = th_4;
        }

        public Double getTh_10() {
            return th_10;
        }

        public void setTh_10(Double th_10) {
            this.th_10 = th_10;
        }

        public Double getTh_24() {
            return th_24;
        }

        public void setTh_24(Double th_24) {
            this.th_24 = th_24;
        }

        public Double getTh_nuit() {
            return th_nuit;
        }

        public void setTh_nuit(Double th_nuit) {
            this.th_nuit = th_nuit;
        }

        public Double getTa_resmoi() {
            return ta_resmoi;
        }

        public void setTa_resmoi(Double ta_resmoi) {
            this.ta_resmoi = ta_resmoi;
        }

        public Double getTa_nres7j() {
            return ta_nres7j;
        }

        public void setTa_nres7j(Double ta_nres7j) {
            this.ta_nres7j = ta_nres7j;
        }

        public Double getTa_moimot() {
            return ta_moimot;
        }

        public void setTa_moimot(Double ta_moimot) {
            this.ta_moimot = ta_moimot;
        }

        public Double getTa_moivel() {
            return ta_moivel;
        }

        public void setTa_moivel(Double ta_moivel) {
            this.ta_moivel = ta_moivel;
        }

        public Double getTv_1h() {
            return tv_1h;
        }

        public void setTv_1h(Double tv_1h) {
            this.tv_1h = tv_1h;
        }

        public String getInfor() {
            return infor;
        }

        public void setInfor(String infor) {
            this.infor = infor;
        }

        public String getPropr() {
            return propr;
        }

        public void setPropr(String propr) {
            this.propr = propr;
        }

        public String getTitul() {
            return titul;
        }

        public void setTitul(String titul) {
            this.titul = titul;
        }

        public String getExploit() {
            return exploit;
        }

        public void setExploit(String exploit) {
            this.exploit = exploit;
        }

        public String getTypgest() {
            return typgest;
        }

        public void setTypgest(String typgest) {
            this.typgest = typgest;
        }

        public String getSecteur() {
            return secteur;
        }

        public void setSecteur(String secteur) {
            this.secteur = secteur;
        }

        public String getInsee() {
            return insee;
        }

        public void setInsee(String insee) {
            this.insee = insee;
        }

        public String getAn_serv() {
            return an_serv;
        }

        public void setAn_serv(String an_serv) {
            this.an_serv = an_serv;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getNb_niv() {
            return nb_niv;
        }

        public void setNb_niv(int nb_niv) {
            this.nb_niv = nb_niv;
        }

        public int getNp_hginf() {
            return np_hginf;
        }

        public void setNp_hginf(int np_hginf) {
            this.np_hginf = np_hginf;
        }

        public int getNp_hgsup() {
            return np_hgsup;
        }

        public void setNp_hgsup(int np_hgsup) {
            this.np_hgsup = np_hgsup;
        }

        public int getNp_fourr() {
            return np_fourr;
        }

        public void setNp_fourr(int np_fourr) {
            this.np_fourr = np_fourr;
        }

        public int getNp_global() {
            return np_global;
        }

        public void setNp_global(int np_global) {
            this.np_global = np_global;
        }

        public int getNp_total() {
            return np_total;
        }

        public void setNp_total(int np_total) {
            this.np_total = np_total;
        }

        public int getNp_pr() {
            return np_pr;
        }

        public void setNp_pr(int np_pr) {
            this.np_pr = np_pr;
        }

        public int getNp_pmr() {
            return np_pmr;
        }

        public void setNp_pmr(int np_pmr) {
            this.np_pmr = np_pmr;
        }

        public int getNp_vle() {
            return np_vle;
        }

        public void setNp_vle(int np_vle) {
            this.np_vle = np_vle;
        }

        public int getNp_mobalt() {
            return np_mobalt;
        }

        public void setNp_mobalt(int np_mobalt) {
            this.np_mobalt = np_mobalt;
        }

        public int getNp_covoit() {
            return np_covoit;
        }

        public void setNp_covoit(int np_covoit) {
            this.np_covoit = np_covoit;
        }

        public int getNp_stlav() {
            return np_stlav;
        }

        public void setNp_stlav(int np_stlav) {
            this.np_stlav = np_stlav;
        }

        public int getNp_2rmot() {
            return np_2rmot;
        }

        public void setNp_2rmot(int np_2rmot) {
            this.np_2rmot = np_2rmot;
        }

        public int getNp_2rele() {
            return np_2rele;
        }

        public void setNp_2rele(int np_2rele) {
            this.np_2rele = np_2rele;
        }

        public int getNp_veltot() {
            return np_veltot;
        }

        public void setNp_veltot(int np_veltot) {
            this.np_veltot = np_veltot;
        }

        public int getNp_velec() {
            return np_velec;
        }

        public void setNp_velec(int np_velec) {
            this.np_velec = np_velec;
        }

        public String getAdresse() {
            return adresse;
        }

        public void setAdresse(String adresse) {
            this.adresse = adresse;
        }

        public Double getGabari_max() {
            return gabari_max;
        }

        public void setGabari_max(Double gabari_max) {
            this.gabari_max = gabari_max;
        }

        public Double getGabari_std() {
            return gabari_std;
        }

        public void setGabari_std(Double gabari_std) {
            this.gabari_std = gabari_std;
        }

        public String getTa_type() {
            return ta_type;
        }

        public void setTa_type(String ta_type) {
            this.ta_type = ta_type;
        }

        public String getTa_handi() {
            return ta_handi;
        }

        public void setTa_handi(String ta_handi) {
            this.ta_handi = ta_handi;
        }

        public String getSiret() {
            return siret;
        }

        public void setSiret(String siret) {
            this.siret = siret;
        }
    }
}
