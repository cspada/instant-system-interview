package com.instant.system.interview.cspada.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.instant.system.interview.cspada.parking.ParkingData;
import com.instant.system.interview.cspada.parking.provider.NoAvailableProviderException;
import com.instant.system.interview.cspada.parking.provider.ParkingDataProvider;
import com.instant.system.interview.cspada.parking.provider.ProviderCriteria;
import com.instant.system.interview.cspada.parking.provider.ProviderResolver;

/**
 * Provide the REST API endpoints to query parking data.
 */
@RestController
public class ParkingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParkingController.class);

    private ProviderResolver providerResolver;

    public ParkingController(ProviderResolver providerResolver) {
        this.providerResolver = providerResolver;
    }

    @GetMapping("/parkings/{city}")
    public List<ParkingData> listParkings(@PathVariable("city") String city) throws NoAvailableProviderException {
        LOGGER.info("Requesting parking list for city {}", city);
        ProviderCriteria providerCriteria = new ProviderCriteria();
        providerCriteria.setCity(city);
        ParkingDataProvider provider = providerResolver.getProvider(providerCriteria);
        return provider.listParkings();
    }
}
