# Description

Exercice suite à l'entretien de Christophe Spada.

Le choix du Français pour le contenu du README est dans la continuité de l'entretien et des échanges par mail qui se 
sont fait en Français.

## Sujet

Le but de l'exercice est de fournir une API REST à destination d'une application mobile pour que l'usager puisse
trouver les parkings d'une zone avec leur informations associées (nombre de places, nombre de places libres, ...)

# Documentation

## Prérequis

java 11 runtime

## Build

`mvn clean install`

## Execution de l'application

L'application est packagée en jar executable (plugin maven spring-boot)

`java -jar target/cspada-interview-1.0.0-SNAPSHOT.jar`

Par défaut l'application écoute sur le port 8080.
L'application étant une application standard spring-boot il est possible de changer le port de la manière suivante
`java -Dserver.port=8081 -jar target/cspada-interview-1.0.0-SNAPSHOT.jar`

## API REST

`GET /parkings/{ville}`
```json
[
  {
    "position": {
      "latitude": 44.8755207,
      "longitude": -0.5443771
    },
    "id": "CUBPK44",
    "name": "Parc-Relais Brandenburg",
    "freeSpots": 185,
    "totalSpots": 211,
    "state": "CLOSED"
  },
  {
    "position": {
      "latitude": 44.8561424,
      "longitude": -0.5618781
    },
    "id": "CUBPK14",
    "name": "Bord'Eau Village",
    "freeSpots": 235,
    "totalSpots": 364,
    "state": "OPEN"
  },
  ...
  ...
]
```

PS: n'est implementé que la ville de bordeaux `GET /parkings/{ville}`

# Approche et réalisation

## Socle technique

* Application spring-boot
* Projet Maven simple (pas de multi module pour ce besoin)

## Approche

### Etude de l'api de la ville de bordeaux

L'URL suivante faisant partie de l'énnoncé : http://data.lacub.fr/wfs?key=9Y2RU3FTE8&SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&TYPENAME=ST_PARK_P&SRSNAME=EPSG:4326

Je suis allé regarder ce que l'on trouve à l'adresse http://data.lacub.fr. Un appel à cet URL dans un navigateur 
redirige vers la page open-data de la ville de bordeaux. On y trouve l'ensemble de ce que la ville de bordeaux met 
à disposition ainsi que des outils pour forger des requêtes et parcourir ces données.

Au passage j'apprends l'existance de standards que je ne connaissais pas comme les webservice OGC...

Dont l'outil http://sig.bordeaux-metropole.fr/apicub/wxsquery/ qui m'a aidé à comprendre l'utilisation du service ainsi 
que les différents arguments de l'URL donnée dans l'énoncé du problème. Cet outil permet donc de forger des URLs 
en fonction des types d'objets recherchés (ici TYPENAME=ST_PARK_P qui limite la requêtes aux Parkings hors voirie)
ainsi que les filtres qui peuvent être appliqués.
Il semblerait qu'il ne soit pas possible d'appliquer de filtre spatial pour le type ST_PARK_P si j'en crois ma 
compréhension de l'outil. Je pars donc sur l'utilisation de ce service pour récupérer la totalité des données
de la ville sans filtrage par position et rayon (ou )

Je remarque aussi que le service est disponible en JSON. Je décide d'utiliser l'API json, plus lisible et moins
verbeuse que la version xml.
(https://data.bordeaux-metropole.fr/geojson?key=9Y2RU3FTE8&typename=ST_PARK_P&srsname=EPSG:4326&version=1.1.0&request=GetFeature)

### Implémentation

Application spring-boot

Pour répondre au besoin de récupération des données via plusieurs providers, `ProviderResolver` permet de selectionner 
un provider pour récupération des données via différents service en fonction des critères de selection (ici seule la
ville est prise en compte mais on pourrait imaginer d'autres critères comme client, api-key, ...)

Un seul endpoint au plus simple.

Ne sont mappés que quelques champs de la réponse finale (tarifs, exploitants, ... ignorés).

pas de gestion d'erreur spécifique. Seul le fait de ne pas avoir de provider de data est géré via un exception qui
est traitée par spring avec un code retour 

### temps de réalisation

* découverte de l'api open-data et investigations : 2h (timebox car ne connaissant pas le sujet, je me suis laissé
  embarquer dans la découverte de pas mal de sujets annexes)
* Analyse, bootstrap du projet et architecture: 1h
* développement de l'API et tests : 1h

## Sujets non traités

### Remontée de toutes les informations

Toutes les informations (tarifs, url, exploitant, ...) obtenues sur les parking ne sont pas remontées dans 
la réponse du service pour des raisons de manque de connaissance sur le besoin et sur les différentes normes ou
format à utiliser (representation des tarifs par exemple).

### Filtrage geospatial

Aucun filtrage geospatial n'est implémenté. L'API opendata de Bordeaux semble ne pas proposer cette option pour les
parkings. Mais il aurait été intéressant d'ajouter cette possibilité de filtrage dans l'API REST pour restreindre le 
nombre de réponses à une zone dans le cas ou un provider fournirait cette possibilité. 

### Services OGC

Ne connaissant pas le domaine et encore moins les standard, je suppose qu'il existe des librairies qui évite
de créer les beans que j'ai utilisé pour le mapping de la réponse du service.
Il faudrait revoir le code pour introduire un module distinc de client OGC

